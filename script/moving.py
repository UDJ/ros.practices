#!/usr/bin/env python
import math
from math import sin, cos, pi, atan2

import rospy, tf
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Pose, Quaternion, Twist



pub = rospy.Publisher( '/map_position', Odometry, queue_size=100 )
pub_cmd = rospy.Publisher("/cmd_vel", Twist, queue_size=1)
rospy.init_node('test_ekf_static')

rate = rospy.Rate(1)

current_time = rospy.Time.now()
time_ = rospy.Duration(0)
x = y = 0.
th = an = pi/2
mv = Twist()
p = Point
a = 0
while not rospy.is_shutdown():
    
    q = tf.transformations.quaternion_from_euler(0, 0, th)
    '''
    Don't need broadcaster here because
    config/localization_ekf.yaml has
        publish_tf: true # parameter 
    '''
    
    odom = Odometry()
    odom.header.stamp = time_
    odom.header.frame_id = "map"
    odom.pose.pose = Pose(Point(x, y, th), Quaternion(*q))
    
    if time_:
        pub.publish(odom)
        pub_cmd.publish(mv)

        if x < 20 and a:
            x, y = [x + 2, y + 0.02]
            mv.linear.x = 0.1
            print x
        if x >= 20 and a:
        
            x, y = [x - 2, y - 0.02]
            an = atan2 (x, y - 0.02)
            mv.linear.x = -0.1
            print x
        else:
            a = 
    rate.sleep()    
    current_time = rospy.Time.now()
    time_ = current_time - rospy.Duration(3)