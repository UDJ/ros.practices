#!/usr/bin/env python
import sys
import math
import csv

from ferro_msgs.msg import QRStamped
from geometry_msgs.msg import PoseStamped, TransformStamped

EPSILON = sys.float_info.epsilon * 10   # ten time bigger than system epsilon
TAG_SIZE = 0.011                        # tag size
W = 1280                                # Camera frame width
H = 1024                                # Camera frame height
WIDTH = 0.405                           # Bot body width
CAMERA_SHIFT = -(WIDTH/2 + 0.05 - 0.02) # Camera shift by Y axis base robot
map_pose = []                           # global variable



class LocalizationError(Exception):
    pass


class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return '[x = {0}, y = {1}]'.format(self.x, self.y)


class Pose:
    def __init__(self, x, y, yaw):
        self.x = x
        self.y = y
        self.yaw = yaw

    def __str__(self):
        return '[x = {0}, y = {1}, yaw = {2}]'.format(self.x, self.y, self.yaw)

    def distance(self):
        return math.sqrt(self.x ** 2 + self.y ** 2)

class Map():
    def __init__(self, teg, x, y, yaw):
        self.teg = teg
        self.x = x
        self.y = y
        self.yaw = yaw




class QRInfo:
    def __init__(self, code, points):
        self.code = code
        if (len(points) < 5):
            raise Exception("Invalid points array")
        self.vertex = points[slice(4)]
        self.center = points[4]

    def __str__(self):
        return 'QR        \"{0}\"\nVertexes: {1}, {2}, {3}, {4}\nCenter:   {5}'.format(
            self.code,
            self.vertex[0],
            self.vertex[1],
            self.vertex[2],
            self.vertex[3],
            self.center)


def normAngle(angle):
    if (angle > math.pi):
        return angle - 2 * math.pi
    if (angle < math.pi):
        return angle + 2 * math.pi


def tagSizeAndAngle(qr):
    """ Evaluate tag size and angle """
    edge_dx = qr.vertex[0].x - qr.vertex[3].x
    edge_dy = qr.vertex[0].y - qr.vertex[3].y
    edge_length = math.sqrt(edge_dx ** 2 + edge_dy ** 2)
    if edge_length < EPSILON:
        raise LocalizationError("Unknown tag size")
    return (TAG_SIZE / edge_length, math.atan2(edge_dy, edge_dx))


def getBotPose(qr, file):
    """ Evaluate robot position by information about observed QR code """
    # calculate tag size ratio and angle
    (ratio, tagAngle) = tagSizeAndAngle(qr)

    # evaluate bot position in camera frame coordinate system
    # 1. Get camera frame center pose in base_link frame
    center = Pose(0, CAMERA_SHIFT, -math.pi/2)
    # 2. Get tag center pose in base_link frame
    tagInBase = Pose(
        center.x + (qr.center.x - W / 2) * ratio,
        center.y + (qr.center.y - H / 2) * ratio,
        center.yaw + tagAngle)

    tagAngle2 = math.atan2(tagInBase.y, tagInBase.x)

    # find tag position in map and evaluate bot position in map
    if map_pose == None:
        readFile(file)
    getTagLocation(qr.code)
    # evaluate polar coordinates of tag->bot vector
    fullAngleFromTagToBot = normAngle(tagAngle + tagAngle2)
    lengthFromTagToBot = tagInBase.distance()
    
    bx = lengthFromTagToBot * math.cos(fullAngleFromTagToBot) + map_x
    by = lengthFromTagToBot * math.sin(fullAngleFromTagToBot) + map_y

    # evaluate bot pose in map

    return Pose(bx, by, tagAngle)


def getHardcodedTagLocation(code):
    return Pose(10, 10, math.pi / 6)


def getTagLocation(tag):
    if (tag in map_pose.__sizeof__.tag):
        
        return Pose(map_pose.x, map_pose.y,map_pose.yaw)
    # else:
    #     raise LocalizationError(
    #         "Tag with code {0} is not found".format(tag))

def readFile(file):
    # reading from data/map.csv
    with open(file, 'r') as csvDataFile:
        csvReader = csv.reader(csvDataFile)
        for row in csvReader:
            map_pose.append(Map(row[0],float(row[1]),float(row[2]),float(row[3])))
            # return row[0], float(row[1]), float(row[2]), float(row[3])


#def main():
    # pose = getBotPose(QRInfo("2",
    #                          [
    #                              Point(200, 200),
    #                              Point(200, 250),
    #                              Point(150, 250),
    #                              Point(150, 200),
    #                              Point(175, 225)
    #                          ]), getHardcodedTagLocation)
    # print(pose)


# if __name__ == "__main__":
#     main()
